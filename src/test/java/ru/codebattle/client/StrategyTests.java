package ru.codebattle.client;

import org.junit.jupiter.api.Test;
import ru.codebattle.client.alg.Direction;
import ru.codebattle.client.api.BoardPoint;
import ru.codebattle.client.api.GameBoard;
import ru.codebattle.client.api.SnakeAction;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StrategyTests {

//    @Test
//    public void test1() {
//        // given
//        String board = "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼\n" +
//            "☼☼        ☼○☼       ®      ☼$☼\n" +
//            "☼#       ●●      ☼☼☼☼☼       ☼\n" +
//            "☼☼  ○   *ø          ●●       ☼\n" +
//            "☼☼  ●                   ○    ☼\n" +
//            "☼☼           ●              ☼☼\n" +
//            "☼#         ●●     ☼#        $☼\n" +
//            "☼☼      ●●☼☼   ☼   ☼  ☼●    ☼☼\n" +
//            "☼☼ ● ●  ☼     ☼○☼  ●  ●○ ●   ☼\n" +
//            "☼☼ ● ●  ☼○         ☼  ☼●     ☼\n" +
//            "☼☼      ●●☼               ●  ☼\n" +
//            "☼☼           ×> *ø        ☼ ☼☼\n" +
//            "☼☼○            ☼            ○☼\n" +
//            "☼☼    ●      ●☼○☼●   ●●●☼ ☼ ☼☼\n" +
//            "☼#          ●●○$○●●          ☼\n" +
//            "☼☼   ●       ●☼○☼●   ●●●  ®  ☼\n" +
//            "☼☼   ○         ☼   *ø        ☼\n" +
//            "☼☼ ® ●   ●☼ ☼                ☼\n" +
//            "☼☼☼         ●     ●          ☼\n" +
//            "☼☼○      ☼☼●●               ☼☼\n" +
//            "☼☼☼         ☼               ○☼\n" +
//            "☼☼○☼●     ●☼#               ☼☼\n" +
//            "☼☼  ●        ○               ☼\n" +
//            "☼☼                  ☼●☼    ● ☼\n" +
//            "☼☼   ●    ○            ●   ●○☼\n" +
//            "☼☼   ●  ○        ☼●☼#    ╓ ● ☼\n" +
//            "☼#        ●●             ║   ☼\n" +
//            "☼☼               ○       ║   ☼\n" +
//            "☼☼○☼       ●●      ☼○☼   ▼ ☼$☼\n" +
//            "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼";
//
//        GameBoard gameBoard = new GameBoard(board);
//
//        // when
//        SnakeAction action = MainStrategy.resolve(gameBoard);
//
//        // then
//        assertEquals(new SnakeAction(false, Direction.LEFT), action);
//    }

//    @Test
//    public void test2() {
//        String board = "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼\n" +
//            "☼☼        ☼○☼             ♥☼$☼\n" +
//            "☼#        ●      ☼☼☼☼☼    ║  ☼\n" +
//            "☼☼      ☼#          ●●    ║  ☼\n" +
//            "☼☼  ●                     ║  ☼\n" +
//            "☼☼           ●            ║ ☼☼\n" +
//            "☼#         ●●     ☼#      ║  ☼\n" +
//            "☼☼      ●●☼☼   ☼   ☼  ☼●  ║ ☼☼\n" +
//            "☼☼ ● ●  ☼ ®   ☼○☼  ●  ●○ ●║  ☼\n" +
//            "☼☼ ● ●  ☼○         ☼  ☼● ╘╝  ☼\n" +
//            "☼☼      ●●☼               ●  ☼\n" +
//            "☼☼              ☼# ○      ☼ ☼☼\n" +
//            "☼☼          ○  ☼            ○☼\n" +
//            "☼☼    ●      ●☼○☼●   ●●●☼ ☼ ☼☼\n" +
//            "☼#           ●○$○●           ☼\n" +
//            "☼☼   ●       ●☼○☼●   ●●●  ˄  ☼\n" +
//            "☼☼   ○         ☼   ☼#     │  ☼\n" +
//            "☼☼   ●   ●☼ ☼             ¤  ☼\n" +
//            "☼☼☼        ○●                ☼\n" +
//            "☼☼○      ☼☼●●               ☼☼\n" +
//            "☼☼☼         ☼               ○☼\n" +
//            "☼☼○☼●     ●☼#     ●         ☼☼\n" +
//            "☼☼  ●                        ☼\n" +
//            "☼☼○               ● ☼●☼    ● ☼\n" +
//            "☼☼   ●                 ●   ●○☼\n" +
//            "☼☼   ●           ☼●☼#        ☼\n" +
//            "☼#        ●●                 ☼\n" +
//            "☼☼                         ® ☼\n" +
//            "☼☼○☼       ●●      ☼○☼     ☼$☼\n" +
//            "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼";
//
//        GameBoard gameBoard = new GameBoard(board);
//
//        // when
//        SnakeAction action = MainStrategy.resolve(gameBoard);
//
//        // todo: голова пилюля
////        assertPossible(gameBoard, 26, 1);
//
//        // then
//        assertEquals(new SnakeAction(false, Direction.LEFT), action);
//    }
//
//    @Test
//    public void test3() {
//        String board = "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼\n" +
//            "☼☼        ☼○☼              ☼$☼\n" +
//            "☼#        ●    ○ ☼☼☼☼☼       ☼\n" +
//            "☼☼      ☼#       $  ●●       ☼\n" +
//            "☼☼  ●                        ☼\n" +
//            "☼☼          ○●              ☼☼\n" +
//            "☼#          ●     ☼#         ☼\n" +
//            "☼☼      ●●☼☼   ☼   ☼  ☼●  ○ ☼☼\n" +
//            "☼☼ ● ●  ☼   ○○☼○☼  ●  ●○ ●  ○☼\n" +
//            "☼☼ ● ●  ☼○     ˄   ☼  ☼●     ☼\n" +
//            "☼☼      ●●☼    │          ●  ☼\n" +
//            "☼☼             ¤☼#        ☼ ☼☼\n" +
//            "☼☼             ☼            ○☼\n" +
//            "☼☼    ●      ●☼○☼●   ●●●☼ ☼ ☼☼\n" +
//            "☼#           ●○$○●●          ☼\n" +
//            "☼☼   ●       ●☼○☼●   ●●●     ☼\n" +
//            "☼☼   ○         ☼   ☼#        ☼\n" +
//            "☼☼®  ●   ●☼ ☼                ☼\n" +
//            "☼☼☼         ●     ●          ☼\n" +
//            "☼☼○ ○    ☼☼●●               ☼☼\n" +
//            "☼☼☼         ☼      ╘══╗     ○☼\n" +
//            "☼☼○☼●     ●☼#         ║     ☼☼\n" +
//            "☼☼  ●                 ♥ ®    ☼\n" +
//            "☼☼                  ☼●☼    ● ☼\n" +
//            "☼☼   ●                 ●   ● ☼\n" +
//            "☼☼   ●           ☼●☼#      ● ☼\n" +
//            "☼#        ●●                 ☼\n" +
//            "☼☼                          ○☼\n" +
//            "☼☼○☼       ●●      ☼○☼   ○ ☼$☼\n" +
//            "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼";
//
//        GameBoard gameBoard = new GameBoard(board);
//
//        // when
//        SnakeAction action = MainStrategy.resolve(gameBoard);
//    }

    @Test
    public void test4() {
        String board = "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼\n" +
            "☼☼   ®╔═╗                         ☼\n" +
            "☼☼    ▼ ║        ®                ☼\n" +
            "☼#   ☼☼☼║  ☼☼☼    ☼#   ☼☼☼   ☼☼☼  ☼\n" +
            "☼☼   ☼ ╔╝    ☼       ® ☼       ☼  ☼\n" +
            "☼☼   ☼ ║ ● ● ☼         ☼ ● ● ● ☼  ☼\n" +
            "☼☼     ╙                  ○○○     ☼\n" +
            "☼☼         ●    ®  ®     ●○$○●    ☼\n" +
            "☼☼                        ○○○     ☼\n" +
            "☼#   ☼       ☼    ☼#   ☼ ● ● ● ☼  ☼\n" +
            "☼☼   ☼       ☼         ☼       ☼  ☼\n" +
            "☼☼   ☼☼☼   ☼☼☼         ☼☼☼   ☼☼☼  ☼\n" +
            "☼☼                  ®             ☼\n" +
            "☼☼   ☼☼☼   ☼☼☼         ☼☼☼   ☼☼☼  ☼\n" +
            "☼☼   ☼       ☼         ☼       ☼  ☼\n" +
            "☼#   ☼ ● ● ● ☼    ☼#   ☼ ● ● ● ☼  ☼\n" +
            "☼☼        ●               ○○○     ☼\n" +
            "☼☼         ●             ●○$○●    ☼\n" +
            "☼☼                        ○○○     ☼\n" +
            "☼☼   ☼ ●æ● ● ☼         ☼ ● ●®● ☼  ☼\n" +
            "☼☼   ☼  │    ☼         ☼      ®☼  ☼\n" +
            "☼#   ☼☼☼│  ☼☼☼    ☼#   ☼☼☼   ☼☼☼  ☼\n" +
            "☼☼      │                     ®   ☼\n" +
            "☼☼   ☼☼☼│  ☼☼☼         ☼☼☼   ☼☼☼  ☼\n" +
            "☼☼   ☼  │    ☼         ☼      ®☼  ☼\n" +
            "☼☼  ®☼ ●│● ● ☼         ☼ ● ● ● ☼  ☼\n" +
            "☼☼      │○○     ®         ○○○     ☼\n" +
            "☼#      │$○●      ☼#     ●○$○●  ® ☼\n" +
            "☼☼      ˅○○               ○○○     ☼\n" +
            "☼☼   ☼ ● ● ● ☼         ☼ ● ● ● ☼  ☼\n" +
            "☼☼   ☼       ☼       ® ☼       ☼  ☼\n" +
            "☼☼   ☼☼☼   ☼☼☼         ☼☼☼   ☼☼☼  ☼\n" +
            "☼☼      ®                         ☼\n" +
            "☼☼                                ☼\n" +
            "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼";

        GameBoard gameBoard = new GameBoard(board);

        gameBoard.printBoard();

        // when
        System.out.println(gameBoard.getMyHead());

        Main.setPrevDirection(Direction.LEFT);
        Main.setFuryTicks(0);
        SnakeAction action = Main.runStrategy(gameBoard);

        System.out.println(action);

//        MainStrategy.analyzeEmptySpaces()
    }
}
//Context: Context(prevDirection=LEFT, furyTicks=22, head=♥=[9,7], tail=╘=[7,15], body=[═=[10,7], ═=[11,7], ╗=[12,7], ║=[12,8], ║=[12,9], ║=[12,10], ╚=[12,11], ═=[13,11], ╗=[14,11], ╔=[8,12], ═=[9,12], ═=[10,12], ═=[11,12], ═=[12,12], ═=[13,12], ╝=[14,12], ║=[8,13], ║=[8,14], ╝=[8,15]], snakeParts=[[10,7], [11,7], [12,7], [12,8], [12,9], [12,10], [12,11], [13,11], [14,11], [8,12], [9,12], [10,12], [11,12], [12,12], [13,12], [14,12], [8,13], [8,14], [8,15], [7,15]], stones=7, enemyHeads=[], enemyEvilHeads=[[8,4]], enemyBodies=[[5,4], [6,4], [7,4], [5,5], [5,6], [6,6], [7,6], [7,7], [8,7]], bodyParts=[[10,7], [11,7], [12,7], [12,8], [12,9], [12,10], [12,11], [13,11], [14,11], [8,12], [9,12], [10,12], [11,12], [12,12], [13,12], [14,12], [8,13], [8,14], [8,15]])
//    Context: Context(prevDirection=DOWN, furyTicks=0, head=▼=[14,16], tail=╕=[15,14], body=[╔=[14,14], ║=[14,15]], snakeParts=[[14,14], [14,15], [15,14]], stones=1, enemyHeads=[], enemyEvilHeads=[[17,15]], enemyBodies=[[18,12], [18,13], [18,14], [18,15]], bodyParts=[[14,14], [14,15]])
