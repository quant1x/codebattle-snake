package ru.codebattle.client;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import ru.codebattle.client.alg.Direction;
import ru.codebattle.client.api.BoardElement;
import ru.codebattle.client.api.BoardPoint;
import ru.codebattle.client.api.GameBoard;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.codebattle.client.api.BoardElement.BODY_HORIZONTAL;
import static ru.codebattle.client.api.BoardElement.BODY_LEFT_DOWN;
import static ru.codebattle.client.api.BoardElement.BODY_LEFT_UP;
import static ru.codebattle.client.api.BoardElement.BODY_RIGHT_DOWN;
import static ru.codebattle.client.api.BoardElement.BODY_RIGHT_UP;
import static ru.codebattle.client.api.BoardElement.BODY_VERTICAL;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_HORIZONTAL;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_LEFT_DOWN;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_LEFT_UP;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_RIGHT_DOWN;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_RIGHT_UP;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_VERTICAL;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_DOWN;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_EVIL;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_LEFT;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_RIGHT;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_UP;
import static ru.codebattle.client.api.BoardElement.ENEMY_TAIL_END_DOWN;
import static ru.codebattle.client.api.BoardElement.ENEMY_TAIL_END_LEFT;
import static ru.codebattle.client.api.BoardElement.ENEMY_TAIL_END_RIGHT;
import static ru.codebattle.client.api.BoardElement.ENEMY_TAIL_END_UP;
import static ru.codebattle.client.api.BoardElement.HEAD_EVIL;
import static ru.codebattle.client.api.BoardElement.TAIL_END_DOWN;
import static ru.codebattle.client.api.BoardElement.TAIL_END_LEFT;
import static ru.codebattle.client.api.BoardElement.TAIL_END_RIGHT;
import static ru.codebattle.client.api.BoardElement.TAIL_END_UP;
import static ru.codebattle.client.api.BoardElement.TAIL_INACTIVE;

@Getter
@ToString(exclude = {"gameBoard", "barriers"})
public class Context {
    private final GameBoard gameBoard;
    @Getter(value = AccessLevel.PRIVATE)
    private final Direction prevDirection;
    private final int furyTicks;
    @NonNull
    private final Map.Entry<BoardElement, BoardPoint> head;
    private final Map.Entry<BoardElement, BoardPoint> tail;
    private final List<Map.Entry<BoardElement, BoardPoint>> body;
    private final List<BoardPoint> snakeParts;
    private final List<BoardPoint> barriers;
    private final int stones;
    private final List<BoardPoint> enemyHeads;
    private final List<BoardPoint> enemyEvilHeads;
    private final List<BoardPoint> enemyBodies;
    private List<BoardPoint> bodyParts;
    @Setter
    private List<BoardPoint> achievableGoals = Collections.emptyList();

    public Context(GameBoard gameBoard, Direction prevDirection, int furyTicks, int stones) {
        this.prevDirection = prevDirection;
        this.furyTicks = furyTicks;
        this.gameBoard = gameBoard;
        this.head = this.gameBoard.getMyHeadAsTuple();
        this.tail = this.gameBoard.findFirstElementAsTuple(TAIL_END_DOWN, TAIL_END_LEFT, TAIL_END_RIGHT,
            TAIL_END_UP, TAIL_INACTIVE);
        this.body = this.gameBoard.findAllElementsAsTuple(BODY_HORIZONTAL, BODY_VERTICAL, BODY_LEFT_DOWN,
            BODY_LEFT_UP, BODY_RIGHT_DOWN, BODY_RIGHT_UP);
        this.barriers = gameBoard.getBarriers();
        this.snakeParts = collectSnakeParts();
        this.stones = stones;
        this.enemyHeads = gameBoard.findAllElements(ENEMY_HEAD_DOWN, ENEMY_HEAD_LEFT, ENEMY_HEAD_RIGHT, ENEMY_HEAD_UP);
        this.enemyEvilHeads = gameBoard.findAllElements(ENEMY_HEAD_EVIL);
        this.enemyBodies = gameBoard.findAllElements(
            ENEMY_TAIL_END_DOWN, ENEMY_TAIL_END_LEFT, ENEMY_TAIL_END_UP, ENEMY_TAIL_END_RIGHT,
            ENEMY_BODY_HORIZONTAL, ENEMY_BODY_VERTICAL, ENEMY_BODY_LEFT_DOWN, ENEMY_BODY_LEFT_UP,
            ENEMY_BODY_RIGHT_DOWN, ENEMY_BODY_RIGHT_UP
        );
    }

    public BoardPoint getHeadPoint() {
        return head.getValue();
    }

    public BoardElement getHeadElement() {
        return head.getKey();
    }

    public boolean snakeIsEvil() {
        return head.getKey() == HEAD_EVIL && furyTicks > 1;
    }

    public int getSnakeThreatLevel() {
        return getSnakeParts().size() + (snakeIsEvil() ? 100 : 1);
    }

    public BoardElement previousHeadDirection() {
        switch (prevDirection) {
            case RIGHT:
                return BoardElement.HEAD_RIGHT;
            case LEFT:
                return BoardElement.HEAD_LEFT;
            case DOWN:
                return BoardElement.HEAD_DOWN;
            case UP:
                return BoardElement.HEAD_UP;
        }
        return null;
    }

    private List<BoardPoint> collectSnakeParts() {
        List<BoardPoint> snakeParts = body.stream()
            .map(Map.Entry::getValue)
            .collect(Collectors.toList());
        this.bodyParts = List.copyOf(snakeParts);
        if (tail != null) {
            snakeParts.add(tail.getValue());
        }
        return snakeParts;
    }
}
