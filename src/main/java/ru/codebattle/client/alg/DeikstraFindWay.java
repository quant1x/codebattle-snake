package ru.codebattle.client.alg;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.codebattle.client.MainStrategy;
import ru.codebattle.client.api.BoardPoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class DeikstraFindWay {
    private static final List<Direction> DIRECTIONS = Arrays.asList(Direction.UP, Direction.DOWN, Direction.LEFT, Direction.RIGHT);
    private Map<Point, List<Direction>> possibleWays;
    private int size;
    private Possible possible;

    public DeikstraFindWay() {
    }

    public List<Direction> getShortestWay(int size, Point from,
                                          List<MainStrategy.Goal> goals,
                                          Possible possible) {
        this.size = size;
        this.possible = possible;
        setupPossibleWays();

        Map<Point, List<Direction>> paths = getPaths(from);

        List<Direction> shortestPath = new ArrayList<>();
        int curPriority = -1;
        for (MainStrategy.Goal goal : goals) {
            List<Direction> path = paths.get(goal.getPoint());
            if (path == null || path.isEmpty()) {
                continue;
            }
            if (path.size() > goal.getSteps()) {
                continue;
            }
            if (shortestPath.isEmpty() || shortestPath.size() > path.size() ||
                (shortestPath.size() == path.size() && goal.getPriority() > curPriority)) {
                shortestPath = path;
                curPriority = goal.getPriority();
            }
            System.out.println("Move -> " + goal);
        }
        return shortestPath;
    }

    public List<Direction> getShortestWay2(int size, Point from,
                                           List<MainStrategy.Goal> goals,
                                           Possible possible) {
        this.size = size;
        this.possible = possible;
        setupPossibleWays();

        Map<Point, List<Direction>> paths = getPaths(from);

        var resolvedGoalsByRanges = goals.stream()
            .map(goal -> new ResolvedGoal(goal, paths.get(goal.getPoint())))
            .filter(rGoal -> rGoal.getDirections() != null && !rGoal.getDirections().isEmpty())
            .collect(Collectors.groupingBy(
                rGoal -> rGoal.getDirections().size() / 3,
                TreeMap::new,
                Collectors.toList()
            )); // todo 10? 5? 3?

        for (var resolvedGoalByRange : resolvedGoalsByRanges.entrySet()) {
            System.out.println("Process range: " + resolvedGoalByRange.getKey());
            int curPriority = -1;
            List<Direction> resultPath = new ArrayList<>();
            for (var rangeGoal : resolvedGoalByRange.getValue()) {
                List<Direction> path = rangeGoal.getDirections();
                if (path == null || path.isEmpty()) {
                    continue;
                }
                if (path.size() > rangeGoal.getGoal().getSteps()) {
                    continue;
                }
                if (resultPath.isEmpty() ||
//                    (path.size() == 1 && rangeGoal.getGoal().getPriority() > curPriority) ||
                    (rangeGoal.getGoal().getPriority() > curPriority) ||
                    (rangeGoal.getGoal().getPriority() == curPriority && resultPath.size() > path.size())) {
                    resultPath = path;
                    curPriority = rangeGoal.getGoal().getPriority();
                }
                System.out.println("Range goal -> " + rangeGoal.getGoal());
            }
            if (!resultPath.isEmpty()) {
                return resultPath;
            }
        }

        return Collections.emptyList();
    }

    private Map<Point, List<Direction>> getPaths(Point from) {
        Map<Point, List<Direction>> paths = new HashMap<>();
        for (Point point : possibleWays.keySet()) {
            paths.put(point, new LinkedList<>());
        }

        boolean[][] processed = new boolean[size][size];
        LinkedList<Point> toProcess = new LinkedList<>();

        Point current = from;
        do {
            if (current == null) {
                if (toProcess.isEmpty()) { // TODO test me
                    break;
                }
                current = toProcess.remove();
            }
            List<Direction> before = paths.get(current);
            for (Direction direction : possibleWays.get(current)) {
                Point to = direction.change(current);
                if (possible != null) {
                    if (!possible.possible(to)) {
                        continue;
                    }
                }
                if (processed[to.getX()][to.getY()]) {
                    continue;
                }

                List<Direction> directions = paths.get(to);
                if (directions.isEmpty() || directions.size() > before.size() + 1) {
                    directions.addAll(before);
                    directions.add(direction);

                    if (!processed[to.getX()][to.getY()]) {
                        toProcess.add(to);
                    }
                }
            }
            processed[current.getX()][current.getY()] = true;
            current = null;
        } while (!toProcess.isEmpty());

        return paths;
    }

    private void setupPossibleWays() {
        possibleWays = new TreeMap<>();

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                Point from = new BoardPoint(x, y);
                List<Direction> directions = new LinkedList<>();
                for (Direction direction : DIRECTIONS) {
                    if (possible.possible(from, direction)) {
                        directions.add(direction);
                    }
                }
                possibleWays.put(from, directions);
            }
        }
    }

    public Map<Point, List<Direction>> getPossibleWays() {
        return possibleWays;
    }

    public interface Possible {
        boolean possible(Point from, Direction direction);

        boolean possible(Point atWay);
    }

    @Getter
    @RequiredArgsConstructor
    private static class ResolvedGoal {
        private final MainStrategy.Goal goal;
        private final List<Direction> directions;
    }
}
