package ru.codebattle.client;

import ru.codebattle.client.api.BoardElement;

import java.util.Map;

public class GoalPriority {

    public static final Map<BoardElement, Integer> PRIORITIES = Map.ofEntries(
        Map.entry(BoardElement.APPLE, 1),
        Map.entry(BoardElement.GOLD, 6),
        Map.entry(BoardElement.FURY_PILL, 3),
        Map.entry(BoardElement.STONE, 5),
        Map.entry(BoardElement.ENEMY_HEAD_DOWN, 13),
        Map.entry(BoardElement.ENEMY_HEAD_LEFT, 13),
        Map.entry(BoardElement.ENEMY_HEAD_RIGHT, 13),
        Map.entry(BoardElement.ENEMY_HEAD_UP, 13),
        Map.entry(BoardElement.ENEMY_HEAD_EVIL, 13),
        Map.entry(BoardElement.ENEMY_TAIL_END_DOWN, 1),
        Map.entry(BoardElement.ENEMY_TAIL_END_LEFT, 1),
        Map.entry(BoardElement.ENEMY_TAIL_END_UP, 1),
        Map.entry(BoardElement.ENEMY_TAIL_END_RIGHT, 1),
        Map.entry(BoardElement.ENEMY_BODY_HORIZONTAL, 12),
        Map.entry(BoardElement.ENEMY_BODY_VERTICAL, 12),
        Map.entry(BoardElement.ENEMY_BODY_LEFT_DOWN, 12),
        Map.entry(BoardElement.ENEMY_BODY_LEFT_UP, 12),
        Map.entry(BoardElement.ENEMY_BODY_RIGHT_DOWN, 12),
        Map.entry(BoardElement.ENEMY_BODY_RIGHT_UP, 12)
    );
}
