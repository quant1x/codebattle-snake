package ru.codebattle.client;

import ru.codebattle.client.alg.Direction;
import ru.codebattle.client.api.BoardElement;
import ru.codebattle.client.api.BoardPoint;
import ru.codebattle.client.api.SnakeAction;

import java.util.Random;

public class RndStrategy {

    public static SnakeAction resolve(Context context) {
        var random = new Random(System.currentTimeMillis());
        Direction direction;
        BoardPoint changedPoint;
        BoardElement head;
        do {
            direction = Direction.values()[random.nextInt(Direction.onlyDirections().size())];
            changedPoint = direction.change(context.getHead().getValue());
            head = context.getHeadElement() == BoardElement.HEAD_EVIL ?
                context.previousHeadDirection() :
                context.getHeadElement();
        } while (context.getGameBoard().isBarrierAt(changedPoint) || a(direction, head));
        var act = random.nextInt() % 2 == 0;
        return new SnakeAction(act, direction);
    }

    private static boolean a(Direction direction, BoardElement headElement) {
        if ((direction == Direction.DOWN && headElement == BoardElement.HEAD_UP) ||
            (direction == Direction.UP && headElement == BoardElement.HEAD_DOWN) ||
            (direction == Direction.RIGHT && headElement == BoardElement.HEAD_LEFT) ||
            (direction == Direction.LEFT) && headElement == BoardElement.HEAD_RIGHT) {
            return true;
        }
        return false;
    }
}
