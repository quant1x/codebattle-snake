package ru.codebattle.client.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.lang.NotImplementedException;
import ru.codebattle.client.alg.Direction;
import ru.codebattle.client.alg.Point;

@AllArgsConstructor
@Data
public class BoardPoint implements Point {
    private int x;
    private int y;

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void move(Point pt) {
        this.x = pt.getX();
        this.y = pt.getY();
    }

    @Override
    public Point copy() {
        return new BoardPoint(getX(), getY());
    }

    @Override
    public boolean itsMe(Point pt) {
        return pt.getX() == x && pt.getY() == y;
    }

    @Override
    public boolean itsMe(int x, int y) {
        return this.x == x && this.y == y;
    }

    @Override
    public boolean isOutOf(int size) {
        return isOutOfBoard(size);
    }

    @Override
    public boolean isOutOf(int dw, int dh, int size) {
        throw new NotImplementedException("Method [isOutOf(int,int,int)] not implemented");
    }

    @Override
    public double distance(Point point2) {
        throw new NotImplementedException("Method [distance(Point)] not implemented");
    }

    @Override
    public void change(Point delta) {
        throw new NotImplementedException("Method [change(Point)] not implemented");
    }

    @Override
    public void change(Direction direction) {
        throw new NotImplementedException("Method [change(Direction)] not implemented");
    }

    @Override
    public Point relative(Point offset) {
        throw new NotImplementedException("Method [relative(Point)] not implemented");
    }

    @Override
    public int compareTo(Point o) {
        if (o == null) {
            return -1;
        }
        return Integer.compare(this.hashCode(), o.hashCode());
    }

    @Override
    public int hashCode() {
        return x * 1000 + y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BoardPoint that = (BoardPoint) o;
        return x == that.x &&
            y == that.y;
    }

    @Override
    public String toString() {
        return String.format("[%s,%s]", x, y);
    }

    /**
     * Checks is current point on board or out of range.
     *
     * @param boardSize Board size to compare
     */
    public boolean isOutOfBoard(int boardSize) {
        return x >= boardSize || y >= boardSize || x < 0 || y < 0;
    }

    /**
     * Returns new BoardPoint object shifted left to "delta" points
     */
    public BoardPoint shiftLeft(int delta) {
        return new BoardPoint(x - delta, y);
    }

    /**
     * Returns new BoardPoint object shifted left to 1 point
     */
    public BoardPoint shiftLeft() {
        return shiftLeft(1);
    }

    /**
     * Returns new BoardPoint object shifted right to "delta" points
     */
    public BoardPoint shiftRight(int delta) {
        return new BoardPoint(x + delta, y);
    }

    /**
     * Returns new BoardPoint object shifted right to 1 point
     */
    public BoardPoint shiftRight() {
        return shiftRight(1);
    }

    /**
     * Returns new BoardPoint object shifted top "delta" points
     */
    public BoardPoint shiftTop(int delta) {
        return new BoardPoint(x, y - delta);
    }

    /**
     * Returns new BoardPoint object shifted top 1 point
     */
    public BoardPoint shiftTop() {
        return shiftTop(1);
    }

    /**
     * Returns new BoardPoint object shifted bottom "delta" points
     */
    public BoardPoint shiftBottom(int delta) {
        return new BoardPoint(x, y + delta);
    }

    /**
     * Returns new BoardPoint object shifted bottom 1 point
     */
    public BoardPoint shiftBottom() {
        return shiftBottom(1);
    }

    public boolean notEquals(Object o) {
        return !equals(o);
    }
}
