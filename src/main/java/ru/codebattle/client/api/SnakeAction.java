package ru.codebattle.client.api;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.codebattle.client.alg.Direction;

@RequiredArgsConstructor
@Getter
@EqualsAndHashCode
public class SnakeAction {
    private static final String ACT_COMMAND_PREFIX = "ACT,";

    private final boolean act;
    private final Direction direction;

    @Override
    public String toString() {
        var cmd = act ? ACT_COMMAND_PREFIX : "";
        return cmd + direction.toString();
    }
}
