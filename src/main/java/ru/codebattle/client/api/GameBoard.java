package ru.codebattle.client.api;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static ru.codebattle.client.api.BoardElement.APPLE;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_SLEEP;
import static ru.codebattle.client.api.BoardElement.ENEMY_TAIL_INACTIVE;
import static ru.codebattle.client.api.BoardElement.FLYING_PILL;
import static ru.codebattle.client.api.BoardElement.FURY_PILL;
import static ru.codebattle.client.api.BoardElement.GOLD;
import static ru.codebattle.client.api.BoardElement.HEAD_DEAD;
import static ru.codebattle.client.api.BoardElement.HEAD_DOWN;
import static ru.codebattle.client.api.BoardElement.HEAD_EVIL;
import static ru.codebattle.client.api.BoardElement.HEAD_FLY;
import static ru.codebattle.client.api.BoardElement.HEAD_LEFT;
import static ru.codebattle.client.api.BoardElement.HEAD_RIGHT;
import static ru.codebattle.client.api.BoardElement.HEAD_SLEEP;
import static ru.codebattle.client.api.BoardElement.HEAD_UP;
import static ru.codebattle.client.api.BoardElement.START_FLOOR;
import static ru.codebattle.client.api.BoardElement.STONE;
import static ru.codebattle.client.api.BoardElement.TAIL_INACTIVE;
import static ru.codebattle.client.api.BoardElement.WALL;

public class GameBoard {
    private final int size;
    @Getter
    private final String boardString;

    public GameBoard(String boardString) {
        this.boardString = boardString.replace("\n", "");
        this.size = (int) Math.sqrt(boardString.length());
    }

    public int size() {
        return size;
    }

    public BoardPoint getMyHead() {
        return findFirstElement(HEAD_DEAD, HEAD_DOWN, HEAD_UP, HEAD_LEFT, HEAD_RIGHT, HEAD_EVIL,
            HEAD_FLY, HEAD_SLEEP);
    }

    public Map.Entry<BoardElement, BoardPoint> getMyHeadAsTuple() {
        return findFirstElementAsTuple(HEAD_DEAD, HEAD_DOWN, HEAD_UP, HEAD_LEFT, HEAD_RIGHT, HEAD_EVIL,
            HEAD_FLY, HEAD_SLEEP);
    }

    public List<BoardPoint> getWalls() {
        return findAllElements(WALL);
    }

    public List<BoardPoint> getStones() {
        return findAllElements(STONE);
    }

    public boolean isBarrierAt(BoardPoint point) {
        return getBarriers().contains(point);
    }

    public List<BoardPoint> getApples() {
        return findAllElements(APPLE);
    }

    public List<BoardPoint> getGoodStuff() {
        return findAllElements(APPLE, GOLD, FURY_PILL);
    }

    public boolean amIEvil() {
        return findAllElements(HEAD_EVIL).contains(getMyHead());
    }

    public boolean amIFlying() {
        return findAllElements(HEAD_FLY).contains(getMyHead());
    }

    public List<BoardPoint> getFlyingPills() {
        return findAllElements(FLYING_PILL);
    }

    public List<BoardPoint> getFuryPills() {
        return findAllElements(FURY_PILL);
    }

    public List<BoardPoint> getGold() {
        return findAllElements(GOLD);
    }

    public List<BoardPoint> getStartPoints() {
        return findAllElements(START_FLOOR);
    }

    public List<BoardPoint> getBarriers() {
        return findAllElements(WALL, START_FLOOR, ENEMY_HEAD_SLEEP, ENEMY_TAIL_INACTIVE, TAIL_INACTIVE, STONE);
    }

    public boolean hasElementAt(BoardPoint point, BoardElement element) {
        if (point.isOutOfBoard(size())) {
            return false;
        }

        return getElementAt(point) == element;
    }

    public BoardElement getElementAt(BoardPoint point) {
        return getElementAt(getShiftByPoint(point));
    }

    public BoardElement getElementAt(int shift) {
        return BoardElement.valueOf(boardString.charAt(shift));
    }

    public BoardElement getElementAt(int x, int y) {
        return getElementAt(getShiftByPoint(x, y));
    }

    public void printBoard() {
        for (int i = 0; i < size(); i++) {
            System.out.println(boardString.substring(i * size(), size() * (i + 1)));
        }
    }

    public BoardPoint findElement(BoardElement elementType) {
        for (int i = 0; i < size() * size(); i++) {
            BoardPoint pt = getPointByShift(i);
            if (hasElementAt(pt, elementType)) {
                return pt;
            }
        }
        return null;
    }

    public BoardPoint findFirstElement(BoardElement... elementTypes) {
        var tuple = findFirstElementAsTuple(elementTypes);
        if (tuple == null) {
            return null;
        }
        return tuple.getValue();
    }

    public Map.Entry<BoardElement, BoardPoint> findFirstElementAsTuple(BoardElement... elementTypes) {
        int shift = 0;
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                BoardElement curElement = getElementAt(shift);
                for (BoardElement element : elementTypes) {
                    if (curElement == element) {
                        return Map.entry(curElement, new BoardPoint(x, y));
                    }
                }
                shift ++;
            }
        }
        return null;
    }

    public List<Map.Entry<BoardElement, BoardPoint>> findAllElementsAsTuple(BoardElement... elementTypes) {
        List<Map.Entry<BoardElement, BoardPoint>> result = new ArrayList<>();
        int shift = 0;
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                BoardElement curElement = getElementAt(shift);
                for (BoardElement element : elementTypes) {
                    if (curElement == element) {
                        result.add(Map.entry(element, new BoardPoint(x, y)));
                    }
                }
                shift ++;
            }
        }
        return result;
    }

    public List<BoardPoint> findAllElements(BoardElement... elementTypes) {
        return findAllElementsAsTuple(elementTypes).stream().map(Map.Entry::getValue).collect(Collectors.toList());
    }

    public boolean hasElementAt(BoardPoint point, BoardElement... elements) {
        return Arrays.stream(elements).anyMatch(element -> hasElementAt(point, element));
    }

    private int getShiftByPoint(BoardPoint point) {
        return point.getY() * size() + point.getX();
    }

    public int getShiftByPoint(int x, int y) {
        return x * size() + y;
    }

    private BoardPoint getPointByShift(int shift) {
        return new BoardPoint(shift % size(), shift / size());
    }
}
