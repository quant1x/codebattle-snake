package ru.codebattle.client;

import lombok.Setter;
import ru.codebattle.client.alg.Direction;
import ru.codebattle.client.api.GameBoard;
import ru.codebattle.client.api.SnakeAction;

import java.io.IOException;
import java.net.URISyntaxException;

import static ru.codebattle.client.api.BoardElement.FURY_PILL;
import static ru.codebattle.client.api.BoardElement.HEAD_EVIL;
import static ru.codebattle.client.api.BoardElement.HEAD_SLEEP;
import static ru.codebattle.client.api.BoardElement.STONE;

public class Main {

    private static final String SERVER_ADDRESS =
        "http://codebattle-pro-2020s1.westeurope.cloudapp.azure.com/codenjoy-contest/board/player/ayczfvmdijy31syq4nhn?code=2683905691570543323&gameName=snakebattle";
    private static final int FURY_PILL_TICKS = 10;
    @Setter
    private static int furyTicks = 0;
    @Setter
    private static Direction prevDirection;
    private static int stones = 0;

    public static void main(String[] args) throws URISyntaxException, IOException {
        SnakeBattleClient client = new SnakeBattleClient(SERVER_ADDRESS);
        client.run(Main::runStrategy);
        System.in.read();
        client.initiateExit();
    }

    public static SnakeAction runStrategy(GameBoard gameBoard) {
        var context = new Context(gameBoard, prevDirection, furyTicks, stones);
        System.out.println("Context: " + context);
        if (context.getHead() == null || context.getHead().getKey() == HEAD_SLEEP) {
            furyTicks = 0;
            stones = 0;
            return new SnakeAction(false, Direction.STOP);
        }

        var action = MainStrategy.resolveAction(context);

        var currDirection = action.getDirection();
        if (context.getHead().getKey() == HEAD_EVIL) {
            furyTicks--;
        } else {
            furyTicks = 0;
        }

        var nextElement = context.getGameBoard().getElementAt(
            currDirection.change(context.getHead().getValue())
        );
        System.out.println("Next element: " + nextElement);
        if (nextElement == FURY_PILL) {
            furyTicks += FURY_PILL_TICKS;
        }
        if (nextElement == STONE) {
            stones++;
        }

        prevDirection = action.getDirection();
        return action;
    }
}
