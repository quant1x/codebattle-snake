package ru.codebattle.client;

import org.apache.commons.lang.ArrayUtils;
import ru.codebattle.client.alg.Direction;
import ru.codebattle.client.alg.Point;
import ru.codebattle.client.api.BoardElement;
import ru.codebattle.client.api.BoardPoint;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiFunction;

public class Utils {

    private static final int THREAT_LEVEL_RANGE = 7; // todo 5?
    private static final int PRIORITIES_RANGE = 3; // todo 5?

    public static BoardElement[] concatBorderElements(BoardElement[] first, BoardElement[] second) {
        return (BoardElement[]) ArrayUtils.addAll(first, second);
    }

    public static int contThreatLevel(BoardPoint point, Context context) {
        return countPoints(point, context, THREAT_LEVEL_RANGE, (_point, _context) -> {
            if (_context.getEnemyHeads().contains(_point) ||
                _context.getEnemyBodies().contains(_point)) {
                return 1;
            }
            if (_context.getEnemyEvilHeads().contains(_point)) {
                return 100;
            }
            return 0;
        });
    }

    public static int countPriorities(BoardPoint goal, Context context) {
        return countPoints(goal, context, PRIORITIES_RANGE,  (_point, _context) -> {
            if (_context.getAchievableGoals().contains(_point)) {
                return GoalPriority.PRIORITIES.get(context.getGameBoard().getElementAt(_point));
            }
            return 0;
        });
    }

    public static void snakeLength(Context context) {
        List<BoardPoint> enemyHeads = context.getEnemyHeads();
    }

    public static int countPoints(BoardPoint point, Context context, int range,
                                  BiFunction<BoardPoint, Context, Integer> countFunction) {
        int counter = 0;
        int half = range / 2;
        int startX = point.getX() - half;
        if (startX < 0) {
            startX = 0;
        }

        int startY = point.getY() - half;
        if (startY < 0) {
            startY = 0;
        }

        int size = context.getGameBoard().size();
        int endX = startX + range;
        if (endX > size) {
            endX = size;
        }

        int endY = startY + range;
        if (endY > size) {
            endY = size;
        }

        for (int y = startY; y < endY; y++) {
            for (int x = startX; x < endX; x++) {
                counter += countFunction.apply(new BoardPoint(x, y), context);
            }
        }
        return counter;
    }

    // bad bad bad
    public static boolean hasEmptySpaceForSnake(BoardPoint point, Context context) {
        boolean[][] processed = new boolean[context.getGameBoard().size()][context.getGameBoard().size()];

        int countEmptySpace = 0;
        LinkedList<Point> toProcess = new LinkedList<>();
        toProcess.add(point);

        do {
            Point current = toProcess.removeFirst();
            for (Direction direction : Direction.onlyDirections()) {
                BoardPoint changedPoint = direction.change(current);
                if (changedPoint.isOutOfBoard(context.getGameBoard().size())) {
                    continue;
                }
                if (processed[changedPoint.getX()][changedPoint.getY()]) {
                    continue;
                }
                if (context.getBarriers().contains(changedPoint) ||
                    context.getBodyParts().contains(changedPoint) ||
                    context.getHeadPoint().equals(changedPoint) ||
                    context.getEnemyEvilHeads().contains(changedPoint) ||
                    (!context.snakeIsEvil() && context.getEnemyHeads().contains(changedPoint))) {
                    // continue
                } else {
                    if (!processed[changedPoint.getX()][changedPoint.getY()]) {
                        toProcess.add(changedPoint);
                        countEmptySpace++;
                    }
                }
                if (countEmptySpace > context.getSnakeParts().size() + 1) {
                    return true;
                }
            }
            processed[current.getX()][current.getY()] = true;
        } while (!toProcess.isEmpty());

        return false;
    }
}
