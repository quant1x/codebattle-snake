package ru.codebattle.client;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.apache.commons.lang.ArrayUtils;
import ru.codebattle.client.alg.DeikstraFindWay;
import ru.codebattle.client.alg.Direction;
import ru.codebattle.client.alg.Point;
import ru.codebattle.client.api.BoardElement;
import ru.codebattle.client.api.BoardPoint;
import ru.codebattle.client.api.SnakeAction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import static ru.codebattle.client.api.BoardElement.APPLE;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_HORIZONTAL;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_LEFT_DOWN;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_LEFT_UP;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_RIGHT_DOWN;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_RIGHT_UP;
import static ru.codebattle.client.api.BoardElement.ENEMY_BODY_VERTICAL;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_DOWN;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_EVIL;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_LEFT;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_RIGHT;
import static ru.codebattle.client.api.BoardElement.ENEMY_HEAD_UP;
import static ru.codebattle.client.api.BoardElement.FURY_PILL;
import static ru.codebattle.client.api.BoardElement.GOLD;
import static ru.codebattle.client.api.BoardElement.START_FLOOR;
import static ru.codebattle.client.api.BoardElement.STONE;
import static ru.codebattle.client.api.BoardElement.WALL;

public class MainStrategy {

    public static final BoardElement[] BASIC_GOALS = new BoardElement[]{
        APPLE, GOLD, FURY_PILL
    };
    public static final BoardElement[] UNDER_PILL_GOALS = new BoardElement[]{
        STONE,
        ENEMY_HEAD_EVIL,
        ENEMY_HEAD_DOWN, ENEMY_HEAD_LEFT, ENEMY_HEAD_RIGHT, ENEMY_HEAD_UP,
//        ENEMY_TAIL_END_DOWN, ENEMY_TAIL_END_LEFT, ENEMY_TAIL_END_UP, ENEMY_TAIL_END_RIGHT,
        ENEMY_BODY_HORIZONTAL, ENEMY_BODY_VERTICAL, ENEMY_BODY_LEFT_DOWN, ENEMY_BODY_LEFT_UP,
        ENEMY_BODY_RIGHT_DOWN, ENEMY_BODY_RIGHT_UP
    };

    public static SnakeAction resolveAction(Context context) {
        var shortestWay = new DeikstraFindWay().getShortestWay2(
            context.getGameBoard().size(),
            context.getHeadPoint(),
            getGoals(context),
            new PossibleImpl(context)
        );
        System.out.println("Ways: " + shortestWay);

        var act = false;
        for (Direction direction : Direction.onlyDirections()) {
            if (context.getStones() > 0 && context.getTail() != null &&
                context.getEnemyHeads().contains(direction.change(context.getTail().getValue()))) {
                act = true;
            }
        }

//        if (context.getStones() > 1) {
//            act = new Random(System.currentTimeMillis()).nextInt() % 2 == 0;
//        }

        return shortestWay.isEmpty() ?
            RndStrategy.resolve(context) :
            new SnakeAction(act, shortestWay.get(0));
    }

    public static List<Goal> getGoals(Context context) {
        var achievableGoals = getAchievableGoals(context);
        context.setAchievableGoals(achievableGoals.stream()
            .map(Map.Entry::getValue).collect(Collectors.toList())
        );

        List<Goal> goals = new ArrayList<>();
        if (!achievableGoals.isEmpty()) {
            for (Map.Entry<BoardElement, BoardPoint> goal : achievableGoals) {
                int steps = Integer.MAX_VALUE;
                if (ArrayUtils.contains(UNDER_PILL_GOALS, goal.getKey())) {
                    steps = context.getFuryTicks() - 1;
                }
                goals.add(new Goal(goal.getValue(), steps,
                    Utils.countPriorities(goal.getValue(), context))
                );
            }
        }
        // System.out.println("Goals: " + goals);
        return goals;
    }

    public static List<Map.Entry<BoardElement, BoardPoint>> getAchievableGoals(Context context) {
        List<Map.Entry<BoardElement, BoardPoint>> allGoals;
        if (context.snakeIsEvil()) {
            allGoals = context.getGameBoard().findAllElementsAsTuple(
                Utils.concatBorderElements(UNDER_PILL_GOALS, BASIC_GOALS)
            );
        } else {
            allGoals = context.getGameBoard().findAllElementsAsTuple(BASIC_GOALS);
        }

        List<Map.Entry<BoardElement, BoardPoint>> achievableGoals = new ArrayList<>();
        for (var goal : allGoals) {
            BoardPoint goalPoint = goal.getValue();
            int countBarriers = 0;

            if (!context.snakeIsEvil() &&
                Utils.contThreatLevel(goal.getValue(), context) > 0
                /*context.getSnakeThreatLevel() < Utils.contThreatLevel(goal.getValue(), context)*/) {
                continue;
            }

            for (Direction direction : Direction.onlyDirections()) {
                var point = direction.change(goalPoint);
                if (context.getBarriers().contains(point) ||
                    context.getBodyParts().contains(point) ||
                    context.getEnemyEvilHeads().contains(point) ||
                    (!context.snakeIsEvil() && context.getEnemyHeads().contains(point))) {
                    countBarriers++;
                }
            }

            if (!Utils.hasEmptySpaceForSnake(goal.getValue(), context)) {
                continue;
            }

            if (countBarriers < 3) {
                achievableGoals.add(goal);
            }
        }

        return achievableGoals;
    }

    @Getter
    @ToString
    @RequiredArgsConstructor
    public static class Goal {
        private final Point point;
        private final int steps;
        private final int priority;
    }

    public static class PossibleImpl implements DeikstraFindWay.Possible {

        private final Context context;

        public PossibleImpl(Context context) {
            this.context = context;
        }

        @Override
        public boolean possible(Point from, Direction direction) {
            List<BoardPoint> barriers;
            List<BoardPoint> enemyBodies;
            List<BoardPoint> enemyHeads;
            List<BoardPoint> enemyEvilHeads;
            BoardElement headElement = context.getHeadElement();
            if (context.snakeIsEvil()) {
                barriers = context.getGameBoard().findAllElements(WALL, START_FLOOR);
                enemyBodies = Collections.emptyList();
                enemyHeads = Collections.emptyList();
                enemyEvilHeads = Collections.emptyList();
                BoardElement previousHeadDirection = context.previousHeadDirection();
                headElement = previousHeadDirection != null ? previousHeadDirection : headElement;
            } else {
                barriers = context.getBarriers();
                enemyBodies = context.getEnemyBodies();
                enemyHeads = context.getEnemyHeads();
                enemyEvilHeads = context.getEnemyEvilHeads();
            }

            BoardPoint changedPoint = direction.change(from);
            // нельзя двигаться в направлении барьера
            if (barriers.contains(changedPoint) ||
                enemyBodies.contains(changedPoint) ||
                enemyHeads.contains(changedPoint) ||
                enemyEvilHeads.contains(changedPoint)) {
                return false;
            }
            // нельзя двигаться в противоположную сторону
            BoardPoint headPoint = context.getHeadPoint();
            if (from.equals(headPoint)) {
                if ((direction == Direction.DOWN && headElement == BoardElement.HEAD_UP) ||
                    (direction == Direction.UP && headElement == BoardElement.HEAD_DOWN) ||
                    (direction == Direction.RIGHT && headElement == BoardElement.HEAD_LEFT) ||
                    (direction == Direction.LEFT) && headElement == BoardElement.HEAD_RIGHT) {
                    return false;
                }
            }
            // не кушаем сами себя
            if (context.getSnakeParts().contains(changedPoint)) {
                return false;
            }

//            if (!context.snakeIsEvil() && countThreatLevel(changedPoint, context) > 0) {
//                return false;
//            }

            return true;
        }

        @Override
        public boolean possible(Point atWay) {
            return atWay.getX() >= 0 && atWay.getY() >= 0 &&
                atWay.getX() < context.getGameBoard().size() && atWay.getY() < context.getGameBoard().size();
        }
    }
}
